--- ANSWERS FOR QUESTIONS ---

1. Since information about current game is stored in "gameState" object, we could simply create new object called "gameTables" which would contain several "gameStates" and additional properties, like "tableName". Example of the structure:

var gameTables {table1: {name: 'Table 1', gameState: {...}, maxPlayers: 10, ...},
		table2: {name: 'Table 2', gameState: {...}, maxPlayers: 6, ...}}

Of course player would have to make a choice between tables during login procedure.

2. We could add authorisation by making a new table in Firebase datastore. This table would contain nicknames, passwords and other data about users. Then during a login procedure, server would start a session for user if he gave proper login data. With this session user would be able to "sit by the table".

--- DODATKOWE INFORMACJE ---

Wi�kszo�� narz�dzi, kt�rymi pos�ugiwa�em si� tworz�c projekt by�a dla mnie nowo�ci�. G��wny tutorial, z kt�rego korzysta�em buduj�c szkielet aplikacji znajduje si� tutaj:

http://start.jcolemorrison.com/building-an-angular-and-express-app-part-1/

Wklejam powy�szy link na wszelki wypadek, gdyby by�y potrzebne jakie� informacje odno�nie konfiguracji.

Zasadniczo m�j kod znajduje si� g��wnie w pliku server/hangman.js (back-end) oraz w katalogu client/app.

Problem, kt�rego nie zd��y�em naprawi� to Bower, kt�ry notorycznie usuwa skrypt klienta socket.io z szablonu (przy ka�dej kompilacji poleceniem grunt). W wersji deweloperskiej ("server/npm test") jest po prostu wklejony "z r�ki" ten skrypt (w index.html) i tam wszysko dzia�a.