'use strict';

/**
 * @ngdoc function
 * @name clientApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the clientApp
 */
  app.controller('LoginCtrl', ['$cookies', '$location', 'socket', function ($cookies, $location, socket) {

  	var ctx = this;

    this.nickname = '';
    this.message = '';
    this.key = Math.random().toString(36).substring(7);

    this.signIn = function() {
    	socket.emit('signin request', { key: ctx.key, nickname: this.nickname });
    	this.message = 'please wait...';
    }

    socket.on(this.key + ' info', function(data) {
    	if (data.type === 'signing_in') {
	    	ctx.message = data.description;
	    	if (data.success) {
	    		$cookies.putObject('loggedPlayer', {id: ctx.key, nickname: ctx.nickname});
	    		$location.path('/game');
	    	}
    	}
    });

    this.initLogin = function() {
    	if ($cookies.getObject('loggedPlayer')) {
    		$location.path('/game');
    	}
    }

    this.initLogin();

  }]);
