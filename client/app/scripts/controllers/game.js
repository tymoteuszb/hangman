'use strict';

/**
 * @ngdoc function
 * @name clientApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the clientApp
 */
  app.controller('GameCtrl', ['$cookies', '$location', 'socket', function ($cookies, $location, socket) {

  	var ctx = this;

  	this.hangmanState = 0;
  	this.wordToGuess = [];
  	ctx.usedLetters = [];
  	this.onlinePlayers = [];
  	this.userInput = '';
  	this.gameActive = false;
  	this.message = '';
  	this.turn = '';
  	this.myturn = false;
  	this.activePlayer = {id: '', nickname: '', points: 0};
  	this.bestPlayers = [];
  	this.winner = '';

  	this.requestRefresh = function() {
  		socket.emit('refresh request');
  	}

  	socket.on('refresh', function (data) {
  		var ok = false;
  		ctx.hangmanState = data.hangmanState;
  		ctx.wordToGuess = data.wordToGuess;
  		ctx.usedLetters = data.usedLetters;
  		ctx.onlinePlayers = data.onlinePlayers;
  		ctx.gameActive = data.active;
  		ctx.turn = ((data.onlinePlayers[data.turn]) ? data.onlinePlayers[data.turn].id : 0);
  		ctx.bestPlayers = (data.bestPlayers || []);
  		data.onlinePlayers.forEach(function (entry) {
  			if ($cookies.getObject('loggedPlayer') && entry.id === $cookies.getObject('loggedPlayer').id) {
  				ok = true;
  				ctx.activePlayer.points = entry.points;
  			}
  		});
  		if ($cookies.getObject('loggedPlayer') && !ok) {
  			// Logout if cookie found, but not logged on server
  			$cookies.remove('loggedPlayer');
  			$location.path('/');
  		}
  	});

  	socket.on((($cookies.getObject('loggedPlayer') !== undefined) ? $cookies.getObject('loggedPlayer').id : '') + ' info', function (data) {
  		ctx.message = data.description;
  	});

  	this.initGame = function() {
  		if ($cookies.getObject('loggedPlayer')) {
	  		this.requestRefresh();
	  		this.activePlayer = $cookies.getObject('loggedPlayer');
  		} else {
  			$location.path('/');
  		}
		};

  	this.tryInput = function() {
  		socket.emit('input request', { key: $cookies.getObject('loggedPlayer').id, input: ctx.userInput.toUpperCase() });
  		ctx.userInput = '';
  	}

  	this.initGame();

  	this.logout = function() {
  		socket.emit('logout request', { id: $cookies.getObject('loggedPlayer').id });
  		$cookies.remove('loggedPlayer');
  		$location.path('/');
  	}

  	this.newGame = function() {
  		socket.emit('newgame request', { key: $cookies.getObject('loggedPlayer').id });
  	}

  }]);