/* Hangman server side */

// Database
var Firebase = require('firebase');

// Some points settings
var settings = { guessedLetter: 1, guessedWord: 5, hangman: -3, wrongLetter: -1, wrongWord: -2 }

// "Random" word generator
function randomWord() {
	var words = ['ANGULAR', 'BOOTSTRAP', 'JAVASCRIPT', 'FRONTEND', 'STYLESHEET', 'PHOTOSHOP', 'WORDPRESS', 'STACKOVERFLOW', 'SYMFONY', 'YEOMAN'];
	return words[Math.floor((Math.random() * 10))];
}

module.exports = function(io) {

	var ctx = this;

	var wordToGuess = '';
	var playerDb = {};
	var finishedGame = false;
	var gameState = { active: false, hangmanState: 0, wordToGuess: [], usedLetters: [], onlinePlayers: [], turn: 0, bestPlayers: [], winner: null };

	// DB connection
	var db = new Firebase('https://apptension-hangman.firebaseio.com');
	var playersRef = db.child('players');

	this.newGame = function(active, players) {
		gameState = { active: active, hangmanState: 0, wordToGuess: [], usedLetters: [], onlinePlayers: players, turn: 0, bestPlayers: [], winner: null };
		wordToGuess = randomWord();
		for (i = 0; i < wordToGuess.length; i++) {
			gameState.wordToGuess.push('');
		}
	}

	this.loadFromDb = function(cb) {
		// Get saved players, tranfsorm to array
		playersRef.on('value', function(snapshot) {
			playerDb = (snapshot.val() || {});
			if (typeof cb === 'function') {
				cb();
			}
		});
	}

	this.loadFromDb();

	io.on('connection', function(socket) {

		/* REQUEST: Sign in (save user on server side -> allow to access the game) */
		socket.on('signin request', function(data) {
			var legal = true;
			var i;
			var pointsUntilNow;

			console.log('Login request from ' + data.nickname);

			gameState.onlinePlayers.forEach(function(entry) {
				if (entry.nickname === data.nickname || data.key === entry.id) {
					legal = false;
				}
			});

			if (!legal) {
				socket.emit(data.key + ' info', { type: 'signing_in', success: false, description: 'There is already a logged user with this nickname!' });
			} else {
				if (gameState.onlinePlayers.length >= 8) {
					socket.emit(data.key + ' info', { type: 'signing_in', success: false, description: 'There are already 8 players in the game!' });
				} else {
					gameState.onlinePlayers.push({ nickname: data.nickname, id: data.key, points: 0, pointsUntilNow: (playerDb[data.nickname]) ? (playerDb[data.nickname].points || 0) : 0 });
					socket.emit(data.key + ' info', { type: 'signing_in', success: true, description: 'Successfully signed in!' });

					console.log(gameState.bestPlayers);

					// Second player logs in - start a new game!
					if (gameState.onlinePlayers.length === 2) {
						console.log('Game changing state to active!');
						ctx.newGame(true, gameState.onlinePlayers);
					}

					io.sockets.emit('refresh', gameState);
				}
			}

		});


		/* REQUEST: Refresh (provide current game state) */
		socket.on('refresh request', function() {

			console.log('Refresh request');
			
			// Refresh best players table
			gameState.bestPlayers = [];
			Object.keys(playerDb).forEach( function(player) {
				gameState.bestPlayers.push({ nickname: player, points: playerDb[player].points });
			});

			io.sockets.emit('refresh', gameState);

		});


		/* REQUEST: Logout from game */
		socket.on('logout request', function(data) {

			console.log('Refresh request');

			var i;

			if (gameState.turn === (gameState.onlinePlayers.length - 1)) { // If last player had his turn, move it to first one
				gameState.turn = 0;
			}

			for (i = 0; i < gameState.onlinePlayers.length; i++) {
				if (data.id === gameState.onlinePlayers[i].id) {
					gameState.onlinePlayers.splice(i, 1);
				}
			}

			if (gameState.onlinePlayers.length < 2) { // If players number lower than 2 - change state to inactive
				gameState.active = false;
			}

			io.sockets.emit('refresh', gameState);

		});

		/* REQUEST: Handle user input during game */
		socket.on('input request', function (data) {

			var i,
				guessed = false,
				tried = false,
				points = 0;

			console.log('Input request');

			if (gameState.active) { // Is game at active state?

				if (data.key !== gameState.onlinePlayers[gameState.turn].id) { // Right turn?

					socket.emit(data.key + ' info', { type: 'letter_guess', guessed: false, description: 'Please wait for your turn!' });

				} else if (!data.input.toLowerCase().match(/^[a-z]+$/)) { // Only letters allowed!

					socket.emit(data.key + ' info', { type: 'letter_guess', guessed: false, description: 'Seems like you\'re using invalid characters!' });

				} else if (data.input.length === wordToGuess.length) { // Word or letter?

					// Guessing a word
					if (data.input === wordToGuess) { // Right word
						socket.emit(data.key + ' info', { type: 'word_guess', guessed: true, description: 'You\'re right!' });
						points = settings.guessedWord;
						gameState.onlinePlayers.forEach(function(entry) { // Point a winner
							if (entry.id === data.key) {
								gameState.winner = entry;
							}
						});
					} else { // Wrong word
						socket.emit(data.key + ' info', { type: 'word_guess', guessed: false, description: 'That\'s not this word!' });
						points = settings.wrongWord;
						gameState.hangmanState++;
						if (gameState.hangmanState >= 9) points = settings.hangman;
					}
					gameState.turn++;
					if (gameState.turn >= gameState.onlinePlayers.length) gameState.turn = 0;

				} else if (data.input.length === 1) { // Guessing a letter

					for (i = 0; i < gameState.usedLetters.length; i++) { // Check if it wasn't already tried
						if (gameState.usedLetters[i] === data.input) {
							tried = true;
						}
					}

					for (i = 0; i < gameState.wordToGuess.length; i++) { // ...also in correct letters
						if (gameState.wordToGuess[i] === data.input) {
							tried = true;
						}
					}

					if (!tried) { // Not tried

						for (i = 0; i < wordToGuess.length; i++) {
							if (wordToGuess.charAt(i) === data.input) {
								guessed = true;
								gameState.wordToGuess[i] = wordToGuess.charAt(i);
							}
						}

						if (guessed) { // Right one
							socket.emit(data.key + ' info', { type: 'letter_guess', guessed: true, description: 'Here you are!' });
							points = settings.guessedLetter;
						} else { // Wrong one
							socket.emit(data.key + ' info', { type: 'letter_guess', guessed: false, description: 'Oh, maybe next time!' });
							points = settings.wrongLetter;
							gameState.usedLetters.push(data.input);
							gameState.hangmanState++;
						}
						gameState.turn++;
						if (gameState.turn >= gameState.onlinePlayers.length) gameState.turn = 0;
						guessed = false;

					} else { // Tried

						console.log(data.key + ' info');
						socket.emit(data.key + ' info', { type: 'letter_guess', guessed: false, description: 'This letter has already beed used!' });

					}

				} else { // Wrong number of letters

					socket.emit(data.key + ' info', { type: 'letter_guess', guessed: false, description: 'Come on... This word has ' + wordToGuess.length + ' letters!' });

				}

				for (i = 0; i < gameState.onlinePlayers.length; i++) {
					if (gameState.onlinePlayers[i].id === data.key) {
						gameState.onlinePlayers[i].points += points;
					}
				}
				points = 0;

				// Close current game if it finished
				if (gameState.hangmanState >= 9 || gameState.wordToGuess.indexOf('') === -1 || gameState.winner != null) {
					for (i = 0; i < gameState.onlinePlayers.length; i++) {
						playersRef.child(gameState.onlinePlayers[i].nickname + '/points').set(gameState.onlinePlayers[i].points + gameState.onlinePlayers[i].pointsUntilNow);
					}
					gameState.wordToGuess = wordToGuess.split('');
					finishedGame = true;
					gameState.active = false;
				}

				io.sockets.emit('refresh', gameState);
			}

		});

		/* REQUEST: New game please! */
		socket.on('newgame request', function (data) {
			if (finishedGame) {
				gameState.onlinePlayers.forEach( function(entry) {
					entry.pointsUntilNow += entry.points;
					entry.points = 0;
				});
				ctx.newGame((gameState.onlinePlayers.length >= 2), gameState.onlinePlayers);
				finishedGame = false;
				ctx.loadFromDb(function() {
					// Refresh best players table
					gameState.bestPlayers = [];
					Object.keys(playerDb).forEach( function(player) {
						gameState.bestPlayers.push({ nickname: player, points: playerDb[player].points });
					});
				});
			}
			io.sockets.emit('refresh', gameState);
		});
	});
}